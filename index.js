// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// index.js is used to setup and configure your bot

// Import required packages
const path = require("path");
const restify = require("restify");

// Import required bot services.
// See https://aka.ms/bot-services to learn more about the different parts of a bot.
const {
  BotFrameworkAdapter,
  ConversationState,
  InputHints,
  MemoryStorage,
  UserState
} = require("botbuilder");

const { CosmosDbStorage } = require("botbuilder-azure");

const {
  FlightBookingRecognizer
} = require("./dialogs/flightBookingRecognizer");

// This bot's main dialog.
const { DialogAndWelcomeBot } = require("./bots/dialogAndWelcomeBot");
const { MainDialog } = require("./dialogs/mainDialog");

// the bot's booking dialog
const { BookingDialog } = require("./dialogs/bookingDialog");
const BOOKING_DIALOG = "bookingDialog";

// Note: Ensure you have a .env file and include LuisAppId, LuisAPIKey and LuisAPIHostName.
const ENV_FILE = path.join(__dirname, ".env");
require("dotenv").config({ path: ENV_FILE });

//Accessors
const CONVERSATION_DATA_PROPERTY = "conversationData";
const USER_PROFILE_PROPERTY = "userProfile";
const CONVERSATION_FLOW_PROPERTY = "conversationFlow";
const DIALOG_STATE_PROPERTY = "dialogState";

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
  appId: process.env.MicrosoftAppId,
  appPassword: process.env.MicrosoftAppPassword
});

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
  // This check writes out errors to console log .vs. app insights.
  // NOTE: In production environment, you should consider logging this to Azure
  //       application insights.
  console.error(`\n [onTurnError] unhandled error: ${error}`);

  // Send a trace activity, which will be displayed in Bot Framework Emulator
  await context.sendTraceActivity(
    "OnTurnError Trace",
    `${error}`,
    "https://www.botframework.com/schemas/error",
    "TurnError"
  );

  // Send a message to the user
  let onTurnErrorMessage = "The bot encounted an error or bug.";
  await context.sendActivity(
    onTurnErrorMessage,
    onTurnErrorMessage,
    InputHints.ExpectingInput
  );
  onTurnErrorMessage =
    "To continue to run this bot, please fix the bot source code.";
  await context.sendActivity(
    onTurnErrorMessage,
    onTurnErrorMessage,
    InputHints.ExpectingInput
  );
  // Clear out state
  await conversationState.delete(context);
};

// Define a state store for your bot. See https://aka.ms/about-bot-state to learn more about using MemoryStorage.
// A bot requires a state store to persist the dialog and user state between messages.
// Check if environment is in DEVELOPMENT OR PRODUCTION mode.
const Environment = process.env.Environment;
let storage = null;
if (Environment === "development") {
  // Else use Memory Storage
  // Use CosmoDB storage
  storage = new MemoryStorage();
} else {
  //CosmoDB storage
  storage = new CosmosDbStorage({
    serviceEndpoint: process.env.DB_SERVICE_ENDPOINT,
    authKey: process.env.AUTH_KEY,
    databaseId: process.env.DATABASE,
    collectionId: process.env.COLLECTION
  });
}

const conversationState = new ConversationState(storage);
const userState = new UserState(storage);

// Create the state property accessors for the conversation data and user profile.
let conversationData = conversationState.createProperty(
  CONVERSATION_DATA_PROPERTY
);
let userProfile = userState.createProperty(USER_PROFILE_PROPERTY);
let conversationFlow = conversationState.createProperty(
  CONVERSATION_FLOW_PROPERTY
);

// If configured, pass in the FlightBookingRecognizer.  (Defining it externally allows it to be mocked for tests)
const { LuisAppId, LuisAPIKey, LuisAPIHostName } = process.env;
const luisConfig = {
  applicationId: LuisAppId,
  endpointKey: LuisAPIKey,
  endpoint: `https://${LuisAPIHostName}`
};

const luisRecognizer = new FlightBookingRecognizer(luisConfig);

// Create the main dialog.
const bookingDialog = new BookingDialog(BOOKING_DIALOG);
const dialog = new MainDialog(luisRecognizer, bookingDialog);
const bot = new DialogAndWelcomeBot(conversationState, userState, dialog);

// Create HTTP server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
  console.log(`\n${server.name} listening to ${server.url}`);
  console.log(
    "\nWorldFish Steering Committee Bot has stasrte. \nWorking in " +
      process.env.Environment +
      " mode."
  );
});

// Listen for incoming activities and route them to your bot main dialog.
server.post("/api/messages", (req, res) => {
  // Route received a request to adapter for processing
  adapter.processActivity(req, res, async turnContext => {
    // route to bot activity handler.
    await bot.run(turnContext);
  });
});
